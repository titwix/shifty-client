# shifty-client

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## Pages

Nuxt create the router automatically, which means you only have to create a beautiful folder structure in "pages".
For mywebsite.fr/user, you'll do:
```bash
pages>users>index.vue
```
and for mywebsite.fr/user/123, you'll do
```bash
pages>users>_id.vue
```
then, you'll have your id paramater in (asyncData)
```javascript
const id = params.id;
```

## SCSS

For the style of the website you have to put all of the scss in style.sccs. For component you must create a file in the component folder (asset/compoment/myNewFile.scss)
and then import it in style.scss. It works the same for pages.

For all the common styles, wrote them directly in the style.scss file (p, h1, h2, h3, button, ...). 

## Components

Nuxt auto import the components, but some times you have to do it by hand, it work like vue.js :
```javascript
import CreateAgent from '~/components/agents/create-agent'

export default {
  components: {
  	CreateAgent
  }
}
```